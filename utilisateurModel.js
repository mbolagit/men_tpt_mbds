// utilisateurModel.js
let mongoose = require('mongoose');
// Setup schema
let utilisateurShema = mongoose.Schema({
    nom : {
        type: String,
        required: true
    },
	prenom : {
        type: String
    },
	login : {
        type: String,
        required: true,
        unique: true,
    },
	pass : {
        type: String,
        required: true
    },
    bv : {
        id : {
            type: String,
            required: true
        },
		val : {
            type: String,
            required: true
        },
		desc : {
            type: String,
            required : true
        }
    }
});
// Export Utilisateur model
let Utilisateur = module.exports = mongoose.model('utilisateur', utilisateurShema);
module.exports.get = function (callback, limit) {
    Utilisateur.find(callback).limit(limit);
}