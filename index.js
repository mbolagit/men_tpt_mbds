// FileName: index.js
// Import express
let express = require('express')
// Import Body parser
let bodyParser = require('body-parser');
// Import Mongoose
let mongoose = require('mongoose');
// Initialize the app
let app = express();

// Import routes
let apiRoutes = require("./api-routes")

// Configure bodyparser to handle post requests
app.use(bodyParser.urlencoded({
     extended: true
}));
app.use(bodyParser.json());

// Connect to Mongoose and set connection variable
//mongoose.connect('mongodb://localhost/resthub');
let uri = 'mongodb+srv://mbola:mbola@cluster0-ar1rm.mongodb.net/test?retryWrites=true';
//let url = 'mongodb://layton:azerty12345@ds253918.mlab.com:53918/jereo';
mongoose.connect(uri);

//let url = 'mongodb://layton:azerty12345@ds253918.mlab.com:53918/jereo';
//let db = mongoose.connection;


// Setup server port
let port = process.env.PORT || 3030;

// Send message for default URL
app.get('/', (req, res) => res.send('Hello World with Express'));

// Use Api routes in the App
app.use('/api', apiRoutes)


// Add the code below to index.js
app.listen(port, function () {
     console.log("Running RestHub on port " + port);
});